# Emulator of trinket protocol

This program emulates the protocol of dialogue 
between a car and a trinket.

# Run

1. Install python with version 3.7 or higher.
2. Install required libraries:
```shell script
pip install -r requirement.txt
```
3. Run the script with the following command:
```shell script
python3 main.py
```

# Examples of work

## Example of opening a door

```
(registration, trinket + car)
30820122300d0609...6424eb0203010001 (car pubkey written to trinket)
30820122300d0609...b56bc70203010001 (trinket pubkey written to car)

(handshake, trinket -> car)
1 (id command)
5fc10bca-1a7b-4872-ad91-cad6d77c8737 (challenge for car)

(challenge, car -> trinket)
1e23bedf-3c0c-4e09-9bc4-67ccd296f28f (challenge for trinket)
336a2188ba0f17e6...d64d7993b218a52c (confirm for car)

(response, trinket -> car)
5bb7f910bbf09024...361118be98ea9b92 (confirm for trinket)

(action, car)
car: check response - ok, OPEN_DOOR
```

## Example of closing a door

```
(registration, trinket + car)
30820122300d0609...7837770203010001 (car pubkey written to trinket)
30820122300d0609...a82c3b0203010001 (trinket pubkey written to car)

(handshake, trinket -> car)
2 (id command)
d168f801-f632-420f-adb4-5f32d2554d64 (challenge for car)

(challenge, car -> trinket)
efbf1b27-e6d4-4a93-af2f-2227a258ddfd (challenge for trinket)
96bf64a20313b16e...c2911bfc1da36c05 (confirm for car)

(response, trinket -> car)
3f742684a4c0cf7c...ebb729f3a5cf1130 (confirm for trinket)

(action, car)
car: check response - ok, CLOSE_DOOR
```
