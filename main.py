import argparse
import json
import uuid
from collections import namedtuple
from enum import Enum

from Crypto.Hash import SHA512
from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5


class Signature:
    def __init__(self):
        private_key = RSA.generate(2048)
        self.__signer = PKCS1_v1_5.new(private_key)
        self.__pubic_key = private_key.publickey()

    def public_key(self):
        return self.__pubic_key

    def generate(self, data):
        h = SHA512.new(json.dumps(data).encode())
        return self.__signer.sign(h)


class SignatureVerifier:
    def __init__(self, public_key):
        self.__verifier = PKCS1_v1_5.new(public_key)

    def verify(self, data, signature):
        h = SHA512.new(json.dumps(data).encode())
        return self.__verifier.verify(h, signature)


SignedData = namedtuple('SignedData', ['data', 'signature'])


class Interlocutor:
    def __init__(self):
        self.__signature = Signature()
        self.__verifier = None
        self.__challenge = None

        self.__reset_challenge()

    def __reset_challenge(self):
        self.__challenge = str(uuid.uuid4())

    def public_key(self):
        return self.__signature.public_key()

    def init_verifier(self, companion_pubkey):
        self.__verifier = SignatureVerifier(companion_pubkey)

    def get_challenge(self):
        return self.__challenge

    def sign_data(self, data) -> SignedData:
        signature = self.__signature.generate(data)
        return SignedData(data, signature)

    def verify_data(self, signed_data: SignedData):
        if not self.__verifier.verify(signed_data.data, signed_data.signature):
            print('Incorrect signature of data!')
            return False

        if signed_data.data.get('confirm') != self.__challenge:
            print('Incorrect confirmation challenge in data!')
            return False

        self.__reset_challenge()
        return True


class Command(Enum):
    OPEN_DOOR = 1
    CLOSE_DOOR = 2


class Car(Interlocutor):
    def __init__(self):
        super().__init__()
        self.__command = None

    def remember_command(self, command):
        self.__command = command

    def do_last_command(self):
        print(f'car: check response - ok, {self.__command.name}')


CommandData = namedtuple('CommandData', ['command', 'challenge'])


class Chat:
    def __init__(self):
        print('\n(registration, trinket + car)')

        self.__car = Car()
        self.__trinket = Interlocutor()
        self.__car.init_verifier(self.__trinket.public_key())
        self.__trinket.init_verifier(self.__car.public_key())

        car_pubkey_bytes = self.__car.public_key().exportKey("DER")
        print(f'{self.__b_to_s(car_pubkey_bytes)} (car pubkey written to trinket)')
        trinket_pubkey_bytes = self.__trinket.public_key().exportKey("DER")
        print(f'{self.__b_to_s(trinket_pubkey_bytes)} (trinket pubkey written to car)')

    @staticmethod
    def __b_to_s(data):
        return f'{data[:8].hex()}...{data[-8:].hex()}'

    def trinket_generate_handshake(self, command: Command) -> CommandData:
        print('\n(handshake, trinket -> car)')

        challenge = self.__trinket.get_challenge()

        print(f'{command.value} (id command)')
        print(f'{challenge} (challenge for car)')
        return CommandData(command, challenge)

    def car_process_handshake(self, handshake_data: CommandData):
        print('\n(challenge, car -> trinket)')

        self.__car.remember_command(handshake_data.command)
        challenge = self.__car.get_challenge()
        data = {'challenge': challenge, 'confirm': handshake_data.challenge}
        signed_data = self.__car.sign_data(data)

        print(f'{challenge} (challenge for trinket)')
        print(f'{self.__b_to_s(signed_data.signature)} (confirm for car)')
        return signed_data

    def trinket_process_challenge(self, challenge_data: SignedData):
        print('\n(response, trinket -> car)')

        if not self.__trinket.verify_data(challenge_data):
            return None
        data = {'confirm': challenge_data.data.get('challenge')}
        signed_data = self.__trinket.sign_data(data)

        print(f'{self.__b_to_s(signed_data.signature)} (confirm for trinket)')
        return signed_data

    def car_do_action(self, response_data: SignedData):
        print('\n(action, car)')

        if not self.__car.verify_data(response_data):
            return False
        self.__car.do_last_command()

        return True


def main():
    chat = Chat()

    handshake_data = chat.trinket_generate_handshake(Command.OPEN_DOOR)
    if not handshake_data:
        return

    challenge_data = chat.car_process_handshake(handshake_data)
    if not challenge_data:
        return

    response_data = chat.trinket_process_challenge(challenge_data)
    if not response_data:
        return

    action_result = chat.car_do_action(response_data)
    if not action_result:
        return


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Emulator of trinket protocol')
    args = parser.parse_args()
    main()
